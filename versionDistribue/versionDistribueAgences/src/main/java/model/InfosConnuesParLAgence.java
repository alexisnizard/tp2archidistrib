package model;

public class InfosConnuesParLAgence {
	/* ATTRIBUTES */
	private int idHotel;
	private String mdp;
	private String nomHotel;
	private String paysHotel;
	private String villeHotel;
	private String rueHotel;
	private String WDSL_swRecherche;
	private String WDSL_swReserve;
	private float pourcentageAdd; //pourcentage supplémentaire appliquée au prix de toutes les chambre de l'ĥôtel
	
	/* CONSTRUCTORS */
	public InfosConnuesParLAgence(int idHotel,String mdp,String nomHotel,String paysHotel,String villeHotel,String rueHotel,String WDSL_swRecherche,String WDSL_swReserve,float pourcentageAdd) {
		this.idHotel=idHotel;
		this.mdp=mdp;
		this.nomHotel=nomHotel;
		this.paysHotel=paysHotel;
		this.villeHotel=villeHotel;
		this.rueHotel=rueHotel;
		this.WDSL_swRecherche=WDSL_swRecherche;
		this.WDSL_swReserve=WDSL_swReserve;
		this.pourcentageAdd=pourcentageAdd;
	}
	
	/* METHODS */
	public int getIdHotel() {
		return idHotel;
	}

	public void setIdHotel(int idHotel) {
		this.idHotel = idHotel;
	}
	public String getNomHotel() {
		return nomHotel;
	}

	public void setNomHotel(String nomHotel) {
		this.nomHotel = nomHotel;
	}

	public String getPaysHotel() {
		return paysHotel;
	}

	public void setPaysHotel(String paysHotel) {
		this.paysHotel = paysHotel;
	}

	public String getVilleHotel() {
		return villeHotel;
	}

	public void setVilleHotel(String villeHotel) {
		this.villeHotel = villeHotel;
	}

	public String getRueHotel() {
		return rueHotel;
	}

	public void setRueHotel(String rueHotel) {
		this.rueHotel = rueHotel;
	}

	public String getWDSL_swRecherche() {
		return WDSL_swRecherche;
	}

	public void setWDSL_swRecherche(String wDSL_swRecherche) {
		WDSL_swRecherche = wDSL_swRecherche;
	}

	public String getWDSL_swReserve() {
		return WDSL_swReserve;
	}

	public void setWDSL_swReserve(String wDSL_swReserve) {
		WDSL_swReserve = wDSL_swReserve;
	}

	public float getPourcentageAdd() {
		return pourcentageAdd;
	}

	public void setPourcentageAdd(float pourcentageAdd) {
		this.pourcentageAdd = pourcentageAdd;
	}

	public String getMdp() {
		return mdp;
	}

	public void setMdp(String mdp) {
		this.mdp = mdp;
	}

}
