package model;

import java.awt.FlowLayout;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Base64;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

import org.apache.commons.io.FileUtils;

public class ConvertImage {

	public String ImageToString(String nomImage) throws IOException {
		byte[] fileContent = FileUtils.readFileToByteArray(new File("src/main/resources/"+nomImage+".jpg"));
		String encodedString = Base64.getEncoder().encodeToString(fileContent);
		return encodedString;
	}
	
	public void StringToImage(String codeImage,String nomImage) throws IOException {
		byte[] decodedBytes = Base64.getDecoder().decode(codeImage);
		FileUtils.writeByteArrayToFile(new File("src/main/resources/"+nomImage+".jpg"), decodedBytes);
	}
	
	public void afficherImage(String nomImage) throws IOException {
				
	    BufferedImage img=ImageIO.read(new File("src/main/resources/"+nomImage+".jpg"));
        ImageIcon icon=new ImageIcon(img);
        JFrame frame=new JFrame(nomImage);
        frame.setLayout(new FlowLayout());
        frame.setSize(1280,720);
        JLabel lbl=new JLabel();
        lbl.setIcon(icon);
        frame.add(lbl);
        frame.setVisible(true);
	}
	
}

