package model;

public class Agence {
	/* ATTRIBUTES */
	private int Id;	
	private String Nom;
	
	/* CONSTRUCTORS */
	public Agence(int id, String nom) {
		super();
		Id = id;
		Nom = nom;
	}

	/* METHODS */
	public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	}

	public String getNom() {
		return Nom;
	}

	public void setNom(String nom) {
		Nom = nom;
	}
}

