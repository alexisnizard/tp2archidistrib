package main;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

import exceptions.AucunHotelTrouve;
import hotel0.AucuneOffreTrouvee_Exception;
import hotel0.ErreurReservation_Exception;
import hotel0.IOException_Exception;
import hotel0.Offre;
import hotel0.WrongIdOrPassword_Exception;
import model.InfosConnuesParLAgence;
import model.ConvertImage;
import model.ConvertLocalDateTime;
import repository.AgenceRepository;

/* L'AGENCE A FORCEMENT CONNAISSANCE : 
 * des hotels auxquels elle est partenaire (noms ,adresses MAIS pas aux informations conernant les chambres (dates de disponibilités ,prix...))
 * des URLS WDSL de leurs webservices
 * du login et du mdp pour y accéder
 * du pourcentage supplémentaire appliqué aux prix des reservartions
 * 
 * Ces données sont codés dans la classe AgenceRepository
 * */

public class Agence1 {
	
	public static String enleverLaSensiCasse(String s) {
		return s.substring(0,1).toUpperCase() + s.substring(1).toLowerCase();
	}
	
	public static void main(String[] args) throws AucunHotelTrouve, AucuneOffreTrouvee_Exception, WrongIdOrPassword_Exception, hotel1.AucuneOffreTrouvee_Exception, hotel1.WrongIdOrPassword_Exception, hotel2.AucuneOffreTrouvee_Exception, hotel2.WrongIdOrPassword_Exception, NumberFormatException, ErreurReservation_Exception, hotel1.ErreurReservation_Exception, hotel2.ErreurReservation_Exception, IOException, IOException_Exception, hotel1.IOException_Exception, hotel2.IOException_Exception {
		
		Scanner sc= new Scanner(System.in);
		Regex rg= new Regex();
		String choix;
		AgenceRepository repo=new AgenceRepository(); //Repository en charge de la persistance des données connues par l'Agence
		ConvertLocalDateTime pDT = new ConvertLocalDateTime(); //Utiliser pour transformer un String en LocalDateTime et inversement
		ConvertImage cI = new ConvertImage(); //Utiliser pour ouvrir l'image
		
		System.out.println("Bienvenue ! ");
		
		while (true) {
			System.out.println("Veuillez sélectionner une option ! ");
			System.out.println("1 - Rechercher une chambre d'hôtel ");
			System.out.println("0 - Exit");
			
			choix=sc.nextLine();
			switch(choix) {
				case "1":
					

/* CHOIX VILLE SEJOUR*/
          System.out.println("Veuillez sélectionner la ville de votre séjour");
          
          String choixVille;
          choixVille=sc.nextLine();
          while (!rg.estAlphabetique(choixVille)) {           
            System.err.println("Mauvaise input /!\\ Veuillez entrer un nom de ville correct ");
            choixVille = sc.nextLine();
            }
          
          /* CHOIX DATE ARRIVEE*/
          System.out.println("Veuillez sélectionner votre date d'arrivée au format JJ/MM/AAAA HH:MM");
          
          String choixDateArrive;
          choixDateArrive=sc.nextLine();
          while (!rg.estUneDate(choixDateArrive)) {
            System.err.println("Mauvaise input /!\\ Veuillez entrer une date existante et au format correct");
            choixDateArrive=sc.nextLine();
          }
          
          String[] separeDateHeure= choixDateArrive.split(" ");
          String[] jjMMaaaa= separeDateHeure[0].split("/");
          String[] hhMM= separeDateHeure[1].split(":");
          LocalDateTime dateArrivee=LocalDateTime.of(Integer.parseInt(jjMMaaaa[2]),Integer.parseInt(jjMMaaaa[1])
              ,Integer.parseInt(jjMMaaaa[0]), Integer.parseInt(hhMM[0]),Integer.parseInt(hhMM[1]));
              
          /* CHOIX DATE DEPART*/
          System.out.println("Veuillez sélectionner votre date de départ au format JJ/MM/AAAA HH:MM");
          
          String choixDateDepart;
          choixDateDepart=sc.nextLine();
          while (!rg.estUneDate(choixDateDepart)) {
            System.err.println("Mauvaise input /!\\ Veuillez entrer une date existante et au format correct");
            choixDateDepart=sc.nextLine();
          }
          
          separeDateHeure= choixDateDepart.split(" ");
          jjMMaaaa= separeDateHeure[0].split("/");
          hhMM= separeDateHeure[1].split(":");
          LocalDateTime dateDepart=LocalDateTime.of(Integer.parseInt(jjMMaaaa[2]),Integer.parseInt(jjMMaaaa[1])
              ,Integer.parseInt(jjMMaaaa[0]), Integer.parseInt(hhMM[0]),Integer.parseInt(hhMM[1]));
          
          while(dateDepart.isBefore(dateArrivee)) {             
            System.err.println("Mauvaise input /!\\ Veuillez entrer une date de départ supérieur à votre date d'arrivée");
            
            choixDateDepart=sc.nextLine();
            while (!rg.estUneDate(choixDateDepart)) {
              System.err.println("Mauvaise input /!\\ Veuillez entrer une date existante et au format correct");
              choixDateDepart=sc.nextLine();
            }
            
            separeDateHeure= choixDateDepart.split(" ");
            jjMMaaaa= separeDateHeure[0].split("/");
            hhMM= separeDateHeure[1].split(":");
            dateDepart=LocalDateTime.of(Integer.parseInt(jjMMaaaa[2]),Integer.parseInt(jjMMaaaa[1])
                ,Integer.parseInt(jjMMaaaa[0]), Integer.parseInt(hhMM[0]),Integer.parseInt(hhMM[1]));
          } 
          
          /* CHOIX NBR PERSONNE A HEBERGER*/
          System.out.println("Veuillez sélectionner le nombre de personne à héberger dans la chambre");
          int nbrPers;
          while (true) {
            try{
              nbrPers= sc.nextInt();
              if (nbrPers<1 || nbrPers>6) {
                throw new IllegalArgumentException();               
              }
              break;                
            }catch(Exception e) {
              System.err.println("Mauvaise input /!\\ Veuillez entrer un nombre de personne positif "
                  + "et réaliste pour une seule chambre");
              sc.nextLine();
            }
          }
					
					/* CONSULTATION DES DISPONIBILITEES AUPRES DES HOTELS PARTNERAIRES  */
					ArrayList<hotel0.Offre> listeDesOffres0 = new ArrayList<>();
					ArrayList<hotel1.Offre> listeDesOffres1 = new ArrayList<>();
					ArrayList<hotel2.Offre> listeDesOffres2 = new ArrayList<>();
					InfosConnuesParLAgence infos0=null;
					InfosConnuesParLAgence infos1=null;
					InfosConnuesParLAgence infos2=null;

					if (!repo.hotelsPartenairesAgence(1,enleverLaSensiCasse(choixVille)).isEmpty()) {			
					
						for(InfosConnuesParLAgence i : repo.hotelsPartenairesAgence(1,enleverLaSensiCasse(choixVille))) {
							
							URL url = new URL(i.getWDSL_swRecherche());	
							switch(i.getIdHotel()) {
								case 0:
									try {
										hotel0.RechercheHotelServiceImplService rechercheImplH0 = new hotel0.RechercheHotelServiceImplService(url);
										hotel0.RechercheHotelService proxyH0 = rechercheImplH0.getRechercheHotelServiceImplPort();	
										listeDesOffres0 = (ArrayList<hotel0.Offre>) proxyH0.recherche(1,i.getMdp(),
												pDT.DateToString(dateArrivee), pDT.DateToString(dateDepart), nbrPers);
										infos0=i;
									}catch (hotel0.AucuneOffreTrouvee_Exception e) {
									}
									break;
	
								case 1:
									try {
										hotel1.RechercheHotelServiceImplService rechercheImplH1 = new hotel1.RechercheHotelServiceImplService(url);
										hotel1.RechercheHotelService proxyH1 = rechercheImplH1.getRechercheHotelServiceImplPort();
										listeDesOffres1 = (ArrayList<hotel1.Offre>) proxyH1.recherche(1,i.getMdp(),
												pDT.DateToString(dateArrivee), pDT.DateToString(dateDepart), nbrPers);
										infos1=i;
									}catch (hotel1.AucuneOffreTrouvee_Exception e) {
									}
									break;
	
								case 2:
									try {
										hotel2.RechercheHotelServiceImplService rechercheImplH2 = new hotel2.RechercheHotelServiceImplService(url);
										hotel2.RechercheHotelService proxyH2 = rechercheImplH2.getRechercheHotelServiceImplPort();
										listeDesOffres2 = (ArrayList<hotel2.Offre>) proxyH2.recherche(1,i.getMdp(),
												pDT.DateToString(dateArrivee), pDT.DateToString(dateDepart), nbrPers);
										infos2=i;
									}catch (hotel2.AucuneOffreTrouvee_Exception e) {
									}
									break;
	
							}
							
						}
					}
					else {
						System.out.println("Aucun hôtel correspondant à vos critères n'a été trouvé chez nos partenaires.");
						System.out.println("Merci et à bientôt !");
						System.exit(0);
						return;
					}
					
					System.out.println("\n======================= RESULTATS TROUVES ===============================\n");
					
					if(infos0 != null) {
						System.out.println("Hôtel : "+infos0.getNomHotel()+" , "+infos0.getVilleHotel()+" "+infos0.getRueHotel());
						System.out.println("Voici ses chambres disponibles :\n");
					
						for (hotel0.Offre of :  listeDesOffres0) {
					    	 System.out.println("Numero de l'offre : "+of.getIdOffre()+", nombre de lits : "+of.getNbrLits()+", dateArrivee : "+
					    of.getDateArrivee()+" ,dateDepart : "+of.getDateDepart()+",prix : "+of.getPrix()*infos0.getPourcentageAdd());
					    }
						System.out.println("\n");
					}
					
					if(infos1 != null) {
						System.out.println("Hôtel : "+infos1.getNomHotel()+" , "+infos1.getVilleHotel()+" "+infos1.getRueHotel());
						System.out.println("Voici ses chambres disponibles :\n");
					
						for (hotel1.Offre of :  listeDesOffres1) {
						  	 System.out.println("Numero de l'offre : "+of.getIdOffre()+", nombre de lits : "+of.getNbrLits()+", dateArrivee : "+
									    of.getDateArrivee()+" ,dateDepart : "+of.getDateDepart()+",prix : "+of.getPrix()*infos1.getPourcentageAdd());
					    }
						System.out.println("\n");
					}
					
					if(infos2 != null) {
						System.out.println("Hôtel : "+infos2.getNomHotel()+" , "+infos2.getVilleHotel()+" "+infos2.getRueHotel());
						System.out.println("Voici ses chambres disponibles :\n");
					
						for (hotel2.Offre of :  listeDesOffres2) {
							 System.out.println("Numero de l'offre : "+of.getIdOffre()+", nombre de lits : "+of.getNbrLits()+", dateArrivee : "+
									    of.getDateArrivee()+" ,dateDepart : "+of.getDateDepart()+",prix : "+of.getPrix()*infos2.getPourcentageAdd());
					    	 
					    }
						System.out.println("\n");
					}
					
					System.out.println("==========================================================================\n");
					sc.nextLine();
					
					System.out.println("Voulez vous affichez les images des chambres d'hôtels trouvées ? [y/n] ");
					String choixAff;
					choixAff=sc.nextLine();
					
					ArrayList<String> choixYN= new ArrayList<>();
					choixYN.addAll(Arrays.asList("Y","y","n","N"));
					
					while(!choixYN.contains(choixAff)) {
						System.err.println("Mauvaise input /!\\ Veuillez répondre y ou n");
						choixAff=sc.nextLine();
					}
					
					switch(choixAff) {
					case "y":
						if(infos0 != null) {
							for (hotel0.Offre of :  listeDesOffres0) {							
								cI.StringToImage(of.getImgEncode(),"chambre"+Integer.toString(of.getIdOffre()));
								cI.afficherImage("chambre"+Integer.toString(of.getIdOffre()));
							}							
						}
						if(infos1 != null) {
							for (hotel1.Offre of :  listeDesOffres1) {
								cI.StringToImage(of.getImgEncode(),"chambre"+Integer.toString(of.getIdOffre()));
								cI.afficherImage("chambre"+Integer.toString(of.getIdOffre()));
							}							
							
						}
						if(infos2 != null) {
							for (hotel2.Offre of :  listeDesOffres2) {
								cI.StringToImage(of.getImgEncode(),"chambre"+Integer.toString(of.getIdOffre()));
								cI.afficherImage("chambre"+Integer.toString(of.getIdOffre()));
							}							
							
						}						
						break;
					case "Y":
						if(infos0 != null) {
							for (hotel0.Offre of :  listeDesOffres0) {							
								cI.StringToImage(of.getImgEncode(),"chambre"+Integer.toString(of.getIdOffre()));
								cI.afficherImage("chambre"+Integer.toString(of.getIdOffre()));
							}							
						}
						if(infos1 != null) {
							for (hotel1.Offre of :  listeDesOffres1) {
								cI.StringToImage(of.getImgEncode(),"chambre"+Integer.toString(of.getIdOffre()));
								cI.afficherImage("chambre"+Integer.toString(of.getIdOffre()));
							}							
							
						}
						if(infos2 != null) {
							for (hotel2.Offre of :  listeDesOffres2) {
								cI.StringToImage(of.getImgEncode(),"chambre"+Integer.toString(of.getIdOffre()));
								cI.afficherImage("chambre"+Integer.toString(of.getIdOffre()));
							}							
							
						}
						break;
					case "n":
						break;
					case "N":
						break;
					}
					
					
					/* RESERVATION PARMIS LES CHOIX PROPROSEES  */
					/* 			CHOIX HOTEL  */
					System.out.println("Veuillez entrer le nom de l'ĥôtel à réserver : ");
					
					String choixHotel;
					choixHotel=sc.nextLine();
					InfosConnuesParLAgence hotelSelec=null;
					
					while(hotelSelec == null) {		
						if(infos0 != null) {
							if (choixHotel.equals(infos0.getNomHotel())){
								hotelSelec=infos0;
							}
						}
						if (infos1 != null) {
							if (choixHotel.equals(infos1.getNomHotel())){
								hotelSelec=infos1;
							}
							
						}
						if (infos2 != null) {
							if (choixHotel.equals(infos2.getNomHotel())){
								hotelSelec=infos2;
							}		
						}
							
						if (hotelSelec == null) {
							System.err.println("Ce nom ne fait pas partie de la liste proposée, êtes-vous sûr d'avoir saisi le bonne orthographe ?"
										+ " Veuillez réessayer une nouvelle fois");
							choixHotel=sc.nextLine();
						}
					}
					
					/* 			CHOIX CHAMBRE  */
					System.out.println("\nVeuillez entrer le numero de la chambre à réserver");
					
					String choixChambre; 
					choixChambre=sc.nextLine();
					boolean sortirb=true;
					String reference=null;
					
					while(sortirb) {	
						try {
							switch (hotelSelec.getIdHotel()) {
								case 0:
									for (hotel0.Offre of :  listeDesOffres0) {
										if (of.getIdOffre() == Integer.parseInt(choixChambre)) {
											sortirb=false;
										}
									}
									break;
								case 1:
									for (hotel1.Offre of :  listeDesOffres1) {
										if (of.getIdOffre() == Integer.parseInt(choixChambre)) {
											sortirb=false;
										}
									}							
									break;
								case 2:
									for (hotel2.Offre of :  listeDesOffres2) {
										if (of.getIdOffre() == Integer.parseInt(choixChambre)) {
											sortirb=false;
										}
									}
									
									break;
							}
						} catch (NumberFormatException e) {
							
						}						
						if (sortirb) {
							System.err.println("Ce numero ne fait pas partie de la liste proposée pour votre hôtel, "
									+ "êtes-vous sûr d'avoir saisi le bon numéro d'offre ?"
									+ " Veuillez réessayer une nouvelle fois");									
							choixChambre=sc.nextLine();
						}
					}
					
					System.out.println("\n=========== Réservartion de l'hôtel \""+hotelSelec.getNomHotel()+"\" offre"
							+ " numéro "+choixChambre+" ============");
					
					System.out.println("Veuillez entrer le nom de la personne principale à héberger");
					
					String nom;
					nom=sc.nextLine();
					while (!rg.estAlphabetique(nom)) {						
						System.err.println("Mauvaise input /!\\ Veuillez entrer un nom correct");
						nom = sc.nextLine();
				    }
					
					System.out.println("Veuillez entrer le prénom de la personne principale à héberger");
					String prenom;
					prenom=sc.nextLine();
					while (!rg.estAlphabetique(prenom)) {						
						System.err.println("Mauvaise input /!\\ Veuillez entrer un prenom correct");
						prenom = sc.nextLine();
				    }
					
					System.out.println("Veuillez entrer le code de votre carte bancaire suivit du cyptogramme "
							+ "et de la date d'expiration au format suivant : XXXXXXXXXXXXXXXX XXX MM/AA");
											
					String codeCB;
					codeCB=sc.nextLine();
					while (!rg.estUnCodeCB(codeCB)) {						
						System.err.println("Mauvaise input /!\\ Veuillez entrer un code de carte bancaire correct "
								+ "suivant ce format : XXXXXXXXXXXXXXXX XXX MM/AA");
						codeCB = sc.nextLine();
				    }				
					
					
					/* APPEL AU WEBSERVICE DE RESERVATION  */
					
					switch (hotelSelec.getIdHotel()) {
						case 0:
							for (hotel0.Offre of :  listeDesOffres0) {
								if (of.getIdOffre() == Integer.parseInt(choixChambre)) {

									URL url = new URL(hotelSelec.getWDSL_swReserve());	
									hotel0.ReserveHotelServiceImplService reserveImplH0 = new hotel0.ReserveHotelServiceImplService(url);
									hotel0.ReserveHotelService proxyReserveH0 = reserveImplH0.getReserveHotelServiceImplPort();	
									reference = proxyReserveH0.reserve(1,hotelSelec.getMdp(),Integer.parseInt(choixChambre),
											nom,prenom,codeCB);
								}
							}
							break;
						case 1:
							for (hotel1.Offre of :  listeDesOffres1) {
								if (of.getIdOffre() == Integer.parseInt(choixChambre)) {
									URL url = new URL(hotelSelec.getWDSL_swReserve());	
									hotel1.ReserveHotelServiceImplService reserveImplH1 = new hotel1.ReserveHotelServiceImplService(url);
									hotel1.ReserveHotelService proxyReserveH1 = reserveImplH1.getReserveHotelServiceImplPort();	
									reference = proxyReserveH1.reserve(1,hotelSelec.getMdp(),Integer.parseInt(choixChambre),
											nom,prenom,codeCB);
								}
							}							
							break;
						case 2:
							for (hotel2.Offre of :  listeDesOffres2) {
								if (of.getIdOffre() == Integer.parseInt(choixChambre)) {
									URL url = new URL(hotelSelec.getWDSL_swReserve());	
									hotel2.ReserveHotelServiceImplService reserveImplH2 = new hotel2.ReserveHotelServiceImplService(url);
									hotel2.ReserveHotelService proxyReserveH2 = reserveImplH2.getReserveHotelServiceImplPort();	
									reference = proxyReserveH2.reserve(1,hotelSelec.getMdp(),Integer.parseInt(choixChambre),
											nom,prenom,codeCB);

								}
							}
							
							break;
					}				
					
					System.out.println("\nRéservation effectuée, voici votre numéro de réference : "+reference);
					
					System.out.println("Merci à bientôt !\n");
					System.exit(0);
					break;
				
					
				case "0":
					sc.close();
					System.out.println("Merci à bientôt !");
					System.exit(0);
					return;
				
				default:
					System.err.println("Mauvaise input /!\\ Veuillez entrer le nombre 1 ou 0");
					break;
			}
			
		}
		
	}

	
	
	
}
