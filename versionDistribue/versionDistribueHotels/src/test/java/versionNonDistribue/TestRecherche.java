package versionNonDistribue;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import exceptions.AucuneOffreTrouvee;
import exceptions.WrongIdOrPassword;
import model.Offre;
import model.ConvertLocalDateTime;
import repository.HotelRepository;
import repository.RechercheHotelRepositoryImpl;


class TestRecherche {
	
	
	HotelRepository repo= new HotelRepository();
	RechercheHotelRepositoryImpl test ;
	ArrayList<Offre> lesOffresRenvoyees;

	ConvertLocalDateTime pDT = new ConvertLocalDateTime();
	
	@Test
	void testRecherche() throws WrongIdOrPassword,AucuneOffreTrouvee, IOException {
		test = new RechercheHotelRepositoryImpl(repo.getHotels().get(0));
		lesOffresRenvoyees=test.recherche(1, "123",pDT.DateToString(LocalDateTime.of(2022, 10, 20, 13, 30)),pDT.DateToString(LocalDateTime.of(2022, 10, 25, 18, 15)), 2);
		 for (Offre of :  lesOffresRenvoyees) {
	    	 System.out.println("[idOffre : "+of.getIdOffre()+" ,dateArrivee : "+
	    of.getDateArrivee()+" ,dateDepart : "+of.getDateDepart()+" ,idAgence : "+of.getIdAgence()+"]");
	    }
	}
	
	@Test
	void testRecherche2() throws WrongIdOrPassword,AucuneOffreTrouvee{
		test = new RechercheHotelRepositoryImpl(repo.getHotels().get(0));
		Exception exception = assertThrows(AucuneOffreTrouvee.class, () -> {
			test.recherche(1, "123", pDT.DateToString(LocalDateTime.of(2022, 10, 20, 13, 30)),pDT.DateToString(LocalDateTime.of(2022, 10, 25, 18, 15)), 22); //une chambre pour 22 personnes
		});		

		assertEquals("Aucune offre ne correspond à vos critères", exception.getMessage());
	}

}
