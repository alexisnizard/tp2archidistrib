package service;


import java.io.IOException;
import java.util.ArrayList;
import javax.jws.WebMethod;
import javax.jws.WebService;
import exceptions.AucuneOffreTrouvee;
import exceptions.WrongIdOrPassword;
import model.Offre;

@WebService
public interface RechercheHotelService {

	@WebMethod
	public ArrayList<Offre> recherche(int idAgence, String mdpAgence, String dateArrive,
			String dateDepart,int nbrPersonne) throws WrongIdOrPassword,AucuneOffreTrouvee,IOException;
}
