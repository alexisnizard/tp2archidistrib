package service;

import java.io.IOException;
import java.util.ArrayList;
import javax.jws.WebService;
import exceptions.AucuneOffreTrouvee;
import exceptions.WrongIdOrPassword;
import model.Hotel;
import model.Offre;
import repository.RechercheHotelRepository;
import repository.RechercheHotelRepositoryImpl;

@WebService(endpointInterface="service.RechercheHotelService")
public class RechercheHotelServiceImpl implements RechercheHotelService{
	

	private Hotel hotel;
	
	public RechercheHotelServiceImpl(Hotel hotel) {
		this.hotel=hotel;
	}
	

	@Override
	public ArrayList<Offre> recherche(int idAgence, String mdpAgence, String dateArrive,
			String dateDepart,int nbrPersonne) throws WrongIdOrPassword,AucuneOffreTrouvee,IOException{
		RechercheHotelRepository repository = new RechercheHotelRepositoryImpl(this.hotel);
		return repository.recherche(idAgence,mdpAgence, dateArrive, dateDepart, nbrPersonne);
	}
	
	
	
}
