package model;

public class EstPartenaire {
	
	/* ATTRIBUTES */
	private Agence agence;
	private String mdp;
	private Hotel hotel;

	/* CONSTRUCTORS */
	public EstPartenaire(Agence agence, String mdp, Hotel hotel) {
		super();
		this.agence = agence;
		this.mdp = mdp;
		this.hotel = hotel;
	}
	
	/* METHODS */
	public Agence getAgence() {
		return agence;
	}
	public void setAgence(Agence agence) {
		this.agence = agence;
	}
	public Hotel getHotel() {
		return hotel;
	}
	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}
	public String getMdp() {
		return mdp;
	}
	public void setMdp(String mdp) {
		this.mdp = mdp;
	}

}
