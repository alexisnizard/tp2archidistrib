package model;

public class Offre {
	
	/* ATTRIBUTES */
	private int idOffre;
	private int nbrLits;
	private String dateArrivee;
	private String dateDepart;
	private float prix;
	private int idAgence;
	private String imgEncode;
	
	/* CONSTRUCTORS*/
	public Offre(int idOffre, int nbrLits, String dateArrivee, String dateDepart, float prix,int idAgence,String imgEncode) {
		super();
		this.idOffre = idOffre;
		this.nbrLits = nbrLits;
		this.dateArrivee = dateArrivee;
		this.dateDepart = dateDepart;
		this.prix = prix;
		this.idAgence=idAgence;
		this.setImgEncode(imgEncode);
		
	}
	
	/* METHODS */
	public int getIdOffre() {
		return idOffre;
	}
	public void setIdOffre(int idOffre) {
		this.idOffre = idOffre;
	}
	public int getNbrLits() {
		return nbrLits;
	}
	public void setNbrLits(int nbrLits) {
		this.nbrLits = nbrLits;
	}
	public String getDateArrivee() {
		return dateArrivee;
	}
	public void setDateArrivee(String dateArrivee) {
		this.dateArrivee = dateArrivee;
	}
	public String getDateDepart() {
		return dateDepart;
	}
	public void setDateDepart(String dateDepart) {
		this.dateDepart = dateDepart;
	}
	public float getPrix() {
		return this.prix;
	}
	public void setPrix(float prix) {
		this.prix = prix;
	}

	public int getIdAgence() {
		return idAgence;
	}

	public void setIdAgence(int idAgence) {
		this.idAgence = idAgence;
	}

	public String getImgEncode() {
		return imgEncode;
	}

	public void setImgEncode(String imgEncode) {
		this.imgEncode = imgEncode;
	}
	
}
