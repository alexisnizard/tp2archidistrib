package repository;

import java.io.IOException;
import java.util.ArrayList;

import exceptions.AucuneOffreTrouvee;
import exceptions.WrongIdOrPassword;
import model.Chambre;
import model.ConvertImage;
import model.Hotel;
import model.Offre;
import model.ConvertLocalDateTime;

public class RechercheHotelRepositoryImpl extends HotelRepository implements RechercheHotelRepository {
	
	ConvertLocalDateTime pDT = new ConvertLocalDateTime();
	ConvertImage cI = new ConvertImage();

	private Hotel hotel;
	
	public RechercheHotelRepositoryImpl(Hotel hotel) {
		this.hotel=hotel;
	}
	
	@Override
	public ArrayList<Offre> recherche(int idAgence, String mdpAgence, String dateArrive,
			String dateDepart,int nbrPersonne)throws WrongIdOrPassword,AucuneOffreTrouvee, IOException {
			
		super.verifConnexion(idAgence,mdpAgence,this.hotel);
		ArrayList<Offre> offresRenvoyees = new ArrayList<Offre>();
		
		for (Chambre c : this.hotel.getListeChambres()) {//Pour chaque chambre de l'hotel
				
			if ((nbrPersonne == 2 && c.getNbrLits()<= 2) || (nbrPersonne == c.getNbrLits())) {//Verif nombre de lits (on renvoit egalement la chambre
																								//si il y a deux personnes et 1 lits)	
				
				
				if (c.estDisponible(pDT.StringToDate(dateArrive),pDT.StringToDate(dateDepart))) {//Verif disponibilitées horaires
						
					//Ajout de l'offre au tableau que la méthode va renvoyé					
					offresRenvoyees.add(new Offre(c.getIdChambre(),c.getNbrLits(),dateArrive,dateDepart,c.getPrix(),idAgence,cI.ImageToString(c.getNomImage())));	
					
					
					//Ajout de l'offre dans les données persistantes	
					
					this.hotel.ajouterOffre(new Offre(c.getIdChambre(),c.getNbrLits(),dateArrive,dateDepart,c.getPrix(),idAgence,cI.ImageToString(c.getNomImage())));
					
				}										
			}				
		}
		
		
		if (!offresRenvoyees.isEmpty()) {
			return offresRenvoyees;
		}
		throw new AucuneOffreTrouvee("Aucune offre ne correspond à vos critères");
		
		
	}

}
