package repository;

import java.util.ArrayList;


import exceptions.AucuneOffreTrouvee;
import exceptions.WrongIdOrPassword;
import model.Offre;
import java.io.IOException;
public interface RechercheHotelRepository {
	
	public ArrayList<Offre> recherche(int idAgence, String mdpAgence, String dateArrive,
			String dateDepart,int nbrPersonne) throws WrongIdOrPassword,AucuneOffreTrouvee,IOException;

}
