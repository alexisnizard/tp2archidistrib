package repository;

import exceptions.ErreurReservation;
import exceptions.WrongIdOrPassword;
import model.Chambre;
import model.Hotel;
import model.Offre;
import model.ConvertLocalDateTime;

import java.util.ArrayList;
import java.util.Random;


public class ReserveHotelRepositoryImpl extends HotelRepository implements ReserveHotelRepository{
	
	private Hotel hotel;
	
	public ReserveHotelRepositoryImpl(Hotel hotel) {
		this.hotel=hotel;
	}
	
	ConvertLocalDateTime pDT = new ConvertLocalDateTime();
	
	
	@Override
	public String reserve(int idAgence, String mdpAgence,int idOffre,String nomClient,String prenomClient,String codeCB) throws WrongIdOrPassword,ErreurReservation {
		
		super.verifConnexion(idAgence,mdpAgence,this.hotel);	
		
		//Creation de la réference
		Random rand = new Random();
		int r;
		String reference="";
		for(int i=0;i<6;i++) {
			r=rand.nextInt(10);
			reference+=Integer.toString(r);
		}

		
		//Obtentions des listes d'offres stocké en mémoire pour notre hôtel
		ArrayList<Offre> lesOffres = this.hotel.getListeOffres();//this.offres.get(this.hotel); //possible grâce à l'overriding du hashcode et equals

		
		boolean trouvee=false;
		Offre offreTrouvee = null;
		
		//Recherche de l'idOffre passé en paramètre dans la liste
		if (!lesOffres.isEmpty()) {
			for(Offre o : lesOffres) {
				if(o.getIdOffre() == idOffre && o.getIdAgence() == idAgence) {
					//L'offre à été trouvé
					trouvee=true;
					offreTrouvee=o;
				}
			}	
		}
				
	
		
		/*C'est bon, on a fait notre appel au webservice reservation de l'hôtel,
		  que l'idOffre ai été trouvé ou non, on supprime toutes les offres proposées à l'Agence de 
		  la liste présente dans les données persistentes*/				
		ArrayList<Offre> nouveauTab = new ArrayList<>(); 
		
		for(Offre o :this.hotel.getListeOffres()) {
			if(o.getIdAgence()!=idAgence) {
				nouveauTab.add(o);
			}
			
		}
		
		this.hotel.setListeOffres(nouveauTab);
		
		
		//On fait la réservation dans la chambre puis on renvoit la référence
		if (trouvee) {
			for(Chambre c : this.hotel.getListeChambres()) {
				if (c.getIdChambre() == offreTrouvee.getIdOffre()) {
					
					//Ajout des dates réservées dans le tableau dateReservee de la chambre
					super.getChambres().get(c.getIdChambre()).addUneReservation(pDT.StringToDate(offreTrouvee.getDateArrivee()), pDT.StringToDate(offreTrouvee.getDateDepart()));
					
					//Envoi de la référence
					return reference;
				}
			}
		}
					
		throw new ErreurReservation("Erreur lors de la réservartion : L'idOffre n'a pas été trouvé");

	
		
	}
	
}

