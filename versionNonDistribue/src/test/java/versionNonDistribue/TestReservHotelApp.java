package versionNonDistribue;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import model.Chambre;
import model.Hotel;
import repository.ReserveHotelAppRepository;

class TestReservHotelApp {
	
	ReserveHotelAppRepository app = new ReserveHotelAppRepository();
	
	@Test
	@DisplayName("Test que les dates de réservations ont bien été ajouté "
			+ "dans la chambre 0 lors de l'initialisation des données ")
	void testDateReservee() {
		
		ArrayList<LocalDateTime> tableauDeDateAObtenir = new ArrayList<LocalDateTime>(Arrays.asList
				(LocalDateTime.of(2022, 11, 20, 13, 30),LocalDateTime.of(2022, 11, 27, 18, 15)));
		
		assertEquals(tableauDeDateAObtenir,app.getHotels().get(0).getListeChambres().get(0).getdatesReservees().get(0));
	}
	
	@Test
	@DisplayName("Test de la méthode recherche")
	void testRecherche() {
		HashMap<Hotel, ArrayList<Chambre>>  hotelsChambres = app.recherche("Montpellier", LocalDateTime.of(2022, 10, 20, 13, 30),
				LocalDateTime.of(2022, 10, 25, 13, 30), 60, 200 , 3, 2);
		System.out.println("/////////////////// JUNIT DEBUGGING METHODE RECHERCHE ///////////////////////");
		
		for (Hotel key : hotelsChambres.keySet()) {
			System.out.println("Voici l'id de l'hôtel : "+key.getIdHotel());
			
			assertEquals("Montpellier",key.getVille());
			assertEquals(3,key.getCategHotel());
			
			for (Chambre c : hotelsChambres.get(key)) {
				System.out.println("Voici l'id de la chambre d'hôtel : "+c.getIdChambre());
				
				assertTrue(60<c.getPrix());
				assertTrue(200>c.getPrix());
				assertTrue(c.estDisponible(LocalDateTime.of(2022, 10, 20, 13, 30), LocalDateTime.of(2022, 10, 25, 13, 30)));
			}
			System.out.println("----------");
		}
		
		System.out.println("/////////////////////////////////////////////////////////////////////////////");
		

	}

}
