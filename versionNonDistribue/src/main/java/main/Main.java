package main;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.Scanner;

import model.Chambre;
import model.Client;
import model.Hotel;
import repository.ReserveHotelAppRepository;

public class Main {
	
	public static String enleverLaSensiCasse(String s) {
		return s.substring(0,1).toUpperCase() + s.substring(1).toLowerCase();
	}

	public static void main(String[] args) {
		
		Scanner sc= new Scanner(System.in);
		Regex rg= new Regex();
		int choix;
		ReserveHotelAppRepository app=new ReserveHotelAppRepository();
		
		System.out.println("Bienvenue ! ");
		
		while (true) {
			System.out.println("Veuillez sélectionner une option ! ");
			System.out.println("1 - Rechercher une chambre d'hôtel ");
			System.out.println("0 - Exit");
			
			try {
				choix= sc.nextInt();
				if (choix == 0 || choix == 1) {
					
					if (choix == 1) {					
						//System.out.println(app.getChambres().get(13).getdatesReservees()); 
					
						/* CHOIX VILLE SEJOUR*/
						System.out.println("Veuillez sélectionner la ville de votre séjour");
						sc.nextLine();
						
						String choixVille;
						choixVille=sc.nextLine();
						while (!rg.estAlphabetique(choixVille)) {						
							System.err.println("Mauvaise input /!\\ Veuillez entrer un nom de ville correct ");
							choixVille = sc.nextLine();
					    }
												
						
						/* CHOIX DATE ARRIVEE*/
						System.out.println("Veuillez sélectionner votre date d'arrivée au format JJ/MM/AAAA HH:MM");
						
						String choixDateArrive;
						choixDateArrive=sc.nextLine();
						while (!rg.estUneDate(choixDateArrive)) {
							System.err.println("Mauvaise input /!\\ Veuillez entrer une date existante et au format correct");
							choixDateArrive=sc.nextLine();
						}
						
						String[] separeDateHeure= choixDateArrive.split(" ");
						String[] jjMMaaaa= separeDateHeure[0].split("/");
						String[] hhMM= separeDateHeure[1].split(":");
						LocalDateTime dateArrivee=LocalDateTime.of(Integer.parseInt(jjMMaaaa[2]),Integer.parseInt(jjMMaaaa[1])
								,Integer.parseInt(jjMMaaaa[0]), Integer.parseInt(hhMM[0]),Integer.parseInt(hhMM[1]));
								
						/* CHOIX DATE DEPART*/
						System.out.println("Veuillez sélectionner votre date de départ au format JJ/MM/AAAA HH:MM");
						
						String choixDateDepart;
						choixDateDepart=sc.nextLine();
						while (!rg.estUneDate(choixDateDepart)) {
							System.err.println("Mauvaise input /!\\ Veuillez entrer une date existante et au format correct");
							choixDateDepart=sc.nextLine();
						}
						
						separeDateHeure= choixDateDepart.split(" ");
						jjMMaaaa= separeDateHeure[0].split("/");
						hhMM= separeDateHeure[1].split(":");
						LocalDateTime dateDepart=LocalDateTime.of(Integer.parseInt(jjMMaaaa[2]),Integer.parseInt(jjMMaaaa[1])
								,Integer.parseInt(jjMMaaaa[0]), Integer.parseInt(hhMM[0]),Integer.parseInt(hhMM[1]));
						
						while(dateDepart.isBefore(dateArrivee)) {							
							System.err.println("Mauvaise input /!\\ Veuillez entrer une date de départ supérieur à votre date d'arrivée");
							
							choixDateDepart=sc.nextLine();
							while (!rg.estUneDate(choixDateDepart)) {
								System.err.println("Mauvaise input /!\\ Veuillez entrer une date existante et au format correct");
								choixDateDepart=sc.nextLine();
							}
							
							separeDateHeure= choixDateDepart.split(" ");
							jjMMaaaa= separeDateHeure[0].split("/");
							hhMM= separeDateHeure[1].split(":");
							dateDepart=LocalDateTime.of(Integer.parseInt(jjMMaaaa[2]),Integer.parseInt(jjMMaaaa[1])
									,Integer.parseInt(jjMMaaaa[0]), Integer.parseInt(hhMM[0]),Integer.parseInt(hhMM[1]));
						}				
						
						/* CHOIX PRIX MINIMUM*/
						System.out.println("Veuillez sélectionner le prix minimum pour votre chambre");
						int prixMin;
						while (true) {
							try{
								prixMin= sc.nextInt();
								break;								
							}catch(InputMismatchException e) {
								System.err.println("Mauvaise input /!\\ Veuillez entrer un nombre");
								sc.nextLine();
							}							
						}
						
						/* CHOIX PRIX MAXIMUM*/
						System.out.println("Veuillez sélectionner le prix maximum pour votre chambre");
						int prixMax;
						while (true) {
							try{
								prixMax= sc.nextInt();
								break;								
							}catch(InputMismatchException e) {
								System.err.println("Mauvaise input /!\\ Veuillez entrer un nombre");
								sc.nextLine();
							}							
						}
						
						while (prixMax<prixMin) {
							System.err.println("Mauvaise input /!\\ Veuillez entrer un prix maximum supérieur au prix minimum");
							
							while (true) {
								try{
									prixMax= sc.nextInt();
									break;								
								}catch(InputMismatchException e) {
									System.err.println("Mauvaise input /!\\ Veuillez entrer un nombre");
									sc.nextLine();
								}							
							}
							
						}
						
						
						/* CHOIX CATEGORIE HOTEL*/
						System.out.println("Veuillez sélectionner la catégorie de votre hôtel entre 1 et 5");
						int catHotel;
						while (true) {
							try{
								catHotel= sc.nextInt();
								if (catHotel<1 || catHotel>5) {
									throw new IllegalArgumentException();								
								}
								break;								
							}catch(Exception e) {
								System.err.println("Mauvaise input /!\\ Veuillez entrer un nombre entre 1 et 5");
								sc.nextLine();
							}
						}
						
						/* CHOIX NBR PERSONNE A HEBERGER*/
						System.out.println("Veuillez sélectionner le nombre de personne à héberger dans la chambre");
						int nbrPers;
						while (true) {
							try{
								nbrPers= sc.nextInt();
								if (nbrPers<1 || nbrPers>6) {
									throw new IllegalArgumentException();								
								}
								break;								
							}catch(Exception e) {
								System.err.println("Mauvaise input /!\\ Veuillez entrer un nombre de personne positif "
										+ "et réaliste pour une seule chambre");
								sc.nextLine();
							}
						}
						
						/*APPEL A LA METHODE RECHERCHE*/
						
						HashMap<Hotel, ArrayList<Chambre>> hotelsChambres = app.recherche(enleverLaSensiCasse(choixVille), dateArrivee,
									dateDepart, prixMin, prixMax , catHotel,nbrPers);
						
						if(hotelsChambres.isEmpty()) {
							System.out.println("\nMalheureusement aucun hôtel correspondant à vos critères n'a été trouvé");
							System.out.println("A bientôt");
						}else {
							System.out.println("\nVoici la liste d'ĥôtels et de chambres correspondant à vos critères trouvé");
							
							for (Hotel key : hotelsChambres.keySet()) {
								System.out.println("Hôtel : "+key.getNom());
								for (Chambre c : hotelsChambres.get(key)) {
									System.out.println("Chambre : "+c.getIdChambre()+", Prix = "+c.getPrix()+" €");
								}
							}
							
							
							/*SELECTION DE L'HOTEL ET DE LA CHAMBRE*/
							System.out.println("\nVeuillez entrer le nom de l'ĥôtel à réserver");
							sc.nextLine();
							
							String choixHotel;
							choixHotel=sc.nextLine();
							boolean sortirboucle=true;
							Hotel recupHotel = null;
							
							while (sortirboucle) {						
								for (Hotel key : hotelsChambres.keySet()) {
									if(key.getNom().equals(choixHotel)) {
										recupHotel=key;
										sortirboucle=false;
									}
								}
								
								if (sortirboucle) {
									System.err.println("Ce nom ne fait pas partie de la liste proposée, êtes-vous sûr d'avoir saisi le bonne orthographe ?"
											+ " Veuillez réessayer une nouvelle fois");
									
									choixHotel=sc.nextLine();
								}
						    }
							
							System.out.println("Veuillez entrer le numero de la chambre à réserver");
							
							String choixChambre; //c'est plus simple et rapide d'utiliser un String et de le cast en Integer après que de faire un try catch
							choixChambre=sc.nextLine();
							sortirboucle=true;
							Chambre recupChambre=null;
							
							while (sortirboucle) {	
								for (Chambre c : hotelsChambres.get(recupHotel)) {
									try {
										if(c.getIdChambre() == Integer.parseInt(choixChambre)) {
											recupChambre=c;
											sortirboucle=false;
										}
									}catch(NumberFormatException e) {
										continue;
									}
								}
								
								if (sortirboucle) {
									System.err.println("Ce numero ne fait pas partie de la liste proposée, "
											+ "êtes-vous sûr d'avoir saisi le bon numéro de chambre ?"
											+ " Veuillez réessayer une nouvelle fois");									
									choixChambre=sc.nextLine();
								}
							}
							
							/*RESERVATION DE LA CHAMBRE*/
							System.out.println("\n=========== Réservartion de l'hôtel \""+recupHotel.getNom()+"\" chambre"
									+ " numéro "+recupChambre.getIdChambre()+" ============");
							
							System.out.println("Veuillez entrer le nom de la personne principale à héberger");
							
							String nom;
							nom=sc.nextLine();
							while (!rg.estAlphabetique(nom)) {						
								System.err.println("Mauvaise input /!\\ Veuillez entrer un nom correct");
								nom = sc.nextLine();
						    }
							
							System.out.println("Veuillez entrer le prénom de la personne principale à héberger");
							String prenom;
							prenom=sc.nextLine();
							while (!rg.estAlphabetique(prenom)) {						
								System.err.println("Mauvaise input /!\\ Veuillez entrer un prenom correct");
								prenom = sc.nextLine();
						    }
							
							System.out.println("Veuillez entrer le code de votre carte bancaire suivit du cyptogramme "
									+ "et de la date d'expiration au format suivant : XXXXXXXXXXXXXXXX XXX MM/AA");
													
							String codeCB;
							codeCB=sc.nextLine();
							while (!rg.estUnCodeCB(codeCB)) {						
								System.err.println("Mauvaise input /!\\ Veuillez entrer un code de carte bancaire correct "
										+ "suivant ce format : XXXXXXXXXXXXXXXX XXX MM/AA");
								codeCB = sc.nextLine();
						    }
							
							/*RESERVARTION*/
							if(app.reserve(new Client(nom,prenom), recupChambre, dateDepart, dateDepart, codeCB)) {
								System.out.println("Votre chambre a été réservée ! Merci et à bientôt");
								System.out.println("\n========================== TICKET ===============================");
								System.out.println("Nom : "+nom);
								System.out.println("Prenom : "+prenom);
								System.out.println("Adresse de l'hôtel : "+recupHotel.getVille()+" "+recupHotel.getRue());
								System.out.println("Hôtel : "+recupHotel.getNom());
								System.out.println("Chambre : "+recupChambre.getIdChambre());
								System.out.println("Prix : "+recupChambre.getPrix()+" €");
								System.out.println("Nombre de personnes à héberger : "+nbrPers);
								System.out.println("Date d'arrivée : "+dateArrivee);
								System.out.println("Date de départ : "+dateDepart);
								System.out.println("================================================================\n");
								
							}else {
								System.err.println("Erreur lors de la réservation de votre chambre");
								System.out.println("\n================================================================\n");
							}
							
						}
						
					}
					else {
						sc.close();
						System.out.println("Merci à bientôt !");
						System.exit(0);
					}					
				}else {
					System.err.println("Mauvaise input /!\\ Veuillez entrer le nombre 1 ou 0");
				}
			}catch (InputMismatchException e){
				System.err.println("Mauvaise input /!\\ Veuillez entrer le nombre 1 ou 0");
				sc.nextLine();
			}

		}
		
		
		

	}

}

