# Agence de voyage et hôtels - Service web SOAP



## Tester la calculatrice

Veuillez importer le projet maven calculatrice dans votre workplace eclipse, puis mettez à jour le pom.xml. 

Changer la version java du build path pour la version 11 ,ainsi que le compiler java en version 1.8. 
Votre environnement est configuré, il ne vous reste plus qu'à lancer la class Main pour tester l'application.

## Tester la version non distribuée
Il est possible de tester notre application :
   - directement depuis une console (linux) en lancant le script bash start.sh disponible à l'intérieur du dossier versionNonDistribue (assurez-vous d'avoir une version de maven installée) 
   - depuis Eclipse en : > important le projet maven versionNonDistribue > lançant la classe Main à l'intérieur du package Main.

Note : Depuis la console, les tests Junit seront directement lancés avant la classe Main. Depuis Eclipse, il faudra soit les lancés manuellement ,soit les lancés avec maven : click droit pom.xml > Run as > Maven test)

## Tester la version distribuée
Il est possible de tester notre application :
   - directement depuis une console (linux) en lançant le script bash startServeur.sh disponible à l'intérieur du dossier versionDistribue, puis de lancer la CLI de chacune des agences via les deux scripts bash startAgence1.sh et startAgence2.sh (assurez-vous d'avoir une version de maven installée et au moins java11).

/!\ Il faut attendre que les serveurs soient lancés avant de lancer les clients, cela peut prendre quelques secondes.
   - depuis Eclipse en : > important les deux projets maven versionDistribueAgences et versionDistribueHotels > lançant d'abord le serveur (package server) puis les CLI Agences (package main).

Note : Pour vous assurer du transfert de nos images à l'exercice 3, vous pouvez vider le dossier resources du projet versionDistribueAgences avant de lancer le serveur afin de bien voir les images apparaître à l'intérieur lors de l'appel au webservice recherche.

## Exemple d'affichage

Ci-dessous, vous trouverez un exemple d'affichage du programme :

<p align="center">
  <img src="exemple.png" alt="Exemple d'affichage du programme" width="800">
</p>
